﻿  <ItemGroup>
    <PackageReference Include="Microsoft.EntityFrameworkCore" Version="6.0.31" />
    <PackageReference Include="Microsoft.EntityFrameworkCore.Design" Version="6.0.31">
      <PrivateAssets>all</PrivateAssets>
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
    </PackageReference>
    <PackageReference Include="Microsoft.EntityFrameworkCore.SqlServer" Version="6.0.31" />
    <PackageReference Include="Microsoft.EntityFrameworkCore.Tools" Version="6.0.31">
      <PrivateAssets>all</PrivateAssets>
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
    </PackageReference>
  </ItemGroup>

</Project>



Licencja na każdy pakiet jest udzielana przez jego właściciela. Firma NuGet nie ponosi odpowiedzialności za pakiety innych firm ani nie udziela na nie żadnych licencji. Niektóre pakiety mogą zawierać zależności podlegające dodatkowym licencjom. Sprawdź adres URL źródła pakietu (kanału informacyjnego), aby określić zależności.

Host konsoli menedżera pakietów w wersji 6.4.0.111

Wpisz „get-help NuGet”, aby wyświetlić wszystkie dostępne polecenia NuGet.

PM> add-migration Create
Build started...
Build succeeded.
To undo this action, use Remove-Migration.
PM> update-database
Build started...
Build succeeded.
Applying migration '20240615111609_Create'.
Done.
PM> 