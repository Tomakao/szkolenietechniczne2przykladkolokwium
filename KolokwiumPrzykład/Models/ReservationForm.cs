﻿namespace KolokwiumPrzykład.Models
{
    public class ReservationForm
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Date { get; set; }
        public int RoomId { get; set; }
    }
}
