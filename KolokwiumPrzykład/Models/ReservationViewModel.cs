﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace KolokwiumPrzykład.Models
{
    public class ReservationViewModel
    {
        public ReservationForm Reservation { get; set; }
        public IEnumerable<SelectListItem> Rooms { get; set; }
    }
}
