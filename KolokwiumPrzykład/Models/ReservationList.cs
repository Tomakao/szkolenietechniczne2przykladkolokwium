﻿namespace KolokwiumPrzykład.Models
{
    public class ReservationList
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Date { get; set; }
        public string Room { get; set; }
    }
}
