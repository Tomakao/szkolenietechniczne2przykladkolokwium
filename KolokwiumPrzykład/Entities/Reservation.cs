﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KolokwiumPrzykład.Entities
{
    [Table("Reservations", Schema = "Kolokwium")]
    public class Reservation
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Date { get; set; }

        [Required]
        public int RoomId { get; set; }
        public Room Room { get; set; }
    }
}
