﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace KolokwiumPrzykład.Entities
{
    [Table("Rooms", Schema="Kolokwium")]
    public class Room
    {
        [Key]
        public int Id {  get; set; }
        [Required]
        public string Name { get; set; }
    }
}
