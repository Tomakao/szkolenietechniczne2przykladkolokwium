﻿using KolokwiumPrzykład.Repository;
using System.Runtime.CompilerServices;

namespace KolokwiumPrzykład.Extensions
{
    public static class KolokwiumExtensions
    {
        public static IServiceCollection AddKolokwiumServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IReservationRepository, ReservationRepository>();
            serviceCollection.AddDbContext<KolokwiumDbContext, KolokwiumDbContext>();
            return serviceCollection;
        }
    }
}
