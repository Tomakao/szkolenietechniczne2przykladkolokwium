﻿using KolokwiumPrzykład.Entities;
using Microsoft.EntityFrameworkCore;
using System.Net.Sockets;

namespace KolokwiumPrzykład
{
    public class KolokwiumDbContext : DbContext
    {
        private IConfiguration _configuration { get; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Reservation> Reservations { get; set; }

        public KolokwiumDbContext(IConfiguration configuration)
            : base()
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"server = 10.200.2.28; Database = kolokwium-dev1-w69047; User Id = stud; Password = wsiz;",
                 //options.UseSqlServer(@"server=(localdb)MSSQLLocalDB;Database = Cinema - dev1;tristed_connection = true;",
                 // options.UseSqlServer("",
                 x => x.MigrationsHistoryTable("__EFMigrationsHistory", "KolokwiumPrzyklad"));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
