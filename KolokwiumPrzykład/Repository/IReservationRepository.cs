﻿using KolokwiumPrzykład.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace KolokwiumPrzykład.Repository
{
    public interface IReservationRepository
    {
        void AddReservation(ReservationForm reservation);
        List<ReservationList> ReadReservations();
        List<SelectListItem> ReadRooms();
    }
}
