﻿using KolokwiumPrzykład.Entities;
using KolokwiumPrzykład.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace KolokwiumPrzykład.Repository
{
    public class ReservationRepository : IReservationRepository
    {
        private readonly KolokwiumDbContext _context;
        public ReservationRepository(KolokwiumDbContext context) 
        {
            _context = context;
        }

        public void AddReservation(ReservationForm reservation)
        {
            var reservationEntity = new Reservation()
            {
                Date = reservation.Date,
                Email = reservation.Email,
                Id = reservation.Id,
                PhoneNumber = reservation.PhoneNumber,
                RoomId = reservation.RoomId,
            };

            _context.Reservations.Add(reservationEntity);
            _context.SaveChanges();
        }

        public List<ReservationList> ReadReservations()
        {
            return _context.Reservations.Select(r => new ReservationList()
                {
                    Date = r.Date,
                    Email = r.Email,
                    Id = r.Id,
                    Room = r.RoomId.ToString(),
                    PhoneNumber = r.PhoneNumber
                }).ToList();
        }
        
        public List<SelectListItem> ReadRooms()
        {
            return _context.Rooms.Select(r => new SelectListItem()
            {
                Text = r.Name,
                Value = r.Id.ToString(),
            }).ToList();
        }
    }
}
