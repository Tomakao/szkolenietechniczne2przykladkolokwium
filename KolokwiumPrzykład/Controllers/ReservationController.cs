﻿using KolokwiumPrzykład.Models;
using KolokwiumPrzykład.Repository;
using Microsoft.AspNetCore.Mvc;

namespace KolokwiumPrzykład.Controllers
{
    public class ReservationController : Controller
    {
        private readonly IReservationRepository _reservationRepository;

        public ReservationController(IReservationRepository reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }
        
        public IActionResult Index()
        {
            var reservations = _reservationRepository.ReadReservations();
            return View(reservations);
        }

        public IActionResult Add() 
        {
            ReservationViewModel model= new ReservationViewModel();
            model.Rooms = _reservationRepository.ReadRooms();
            return View(model);
        }

        [HttpPost]
        public IActionResult Add(ReservationViewModel model)
        {
            _reservationRepository.AddReservation(model.Reservation);
            return RedirectToAction("Index");
        }
    }
}
